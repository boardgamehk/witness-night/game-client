import Card from './card'

export default class Action {
    constructor(scene) {
        let playerCard = new Card(scene);
        let pCard;

        this.drawCard = (obj) => {
            let user = obj.user;
            let num = obj.num;
            let selfZone = this.getUserZone(user);
            let image = 'cyanCardBack';

            for (let i=0;i<num;i++) {
                if (user == scene.you) {
                    image = scene.playerDeck[0].image;
                }
                pCard = playerCard.render(scene.zone[selfZone].x, scene.zone[selfZone].y, image).setInteractive();
                pCard.cardData = scene.playerDeck[0];
                scene.zoneData[selfZone].cards.push(pCard);
                scene.playerDeck.shift();
            }
            scene.zoneData[selfZone].cards[0].x = scene.zoneData[selfZone].cards[0].x - 80;
            scene.zoneData[selfZone].cards[1].x = scene.zoneData[selfZone].cards[0].x + 160;
        }

        this.moveCard = (obj) => {
            let fromField = obj.fromField;
            let toField = obj.toField;
            let cardData = obj.cardData;

            this.destroyCard(fromField, cardData);
            this.createCard(toField, cardData);
            console.log(scene.zoneData[fromField]);
            scene.zoneData[fromField].cards[0].x = scene.zone[fromField].x;

            scene.text.gameDesc.setText(`${scene.zoneData[fromField].name}的${scene.zoneData[fromField].user.name}看見了一個人進入${scene.zoneData[toField].name}`);

            if (scene.zoneData[toField].user.name == scene.you) {
                let obj = {};
                obj.user = scene.you;
                obj.num = 1;
                scene.socket.emit('action', 'drawCard', obj);
            }
            if (toField == 'field7') {
                scene.text.vote.visible = true;
                scene.text.voteNum.visible = true;
                scene.text.gameDesc.visible = false;
                scene.text.allTextInteractive(true);
            }
        }

        this.vote = (obj) => {
            let user = obj.user;
            let field = obj.field;
            let killList = [];
            let highestVote = 0;
            let voteCount = 0;

            scene.zoneData[field].vote++;
            if (user == scene.you) {
                scene.text.allTextInteractive(false);
            }
            for (let f in scene.zoneData) {
                voteCount += scene.zoneData[f].vote;
                if (scene.zoneData[f].vote > highestVote) {
                    killList = [];
                }
                if (scene.zoneData[f].vote >= highestVote) {
                    killList.push(f);
                    highestVote = scene.zoneData[f].vote;
                }
            }
            scene.text.voteNum.setText(`${voteCount}/6`);
            if (voteCount == 6) {
                this.revealCard();
                this.kill(killList);
                scene.text.vote.visible = false;
                scene.text.voteNum.visible = false;
                scene.text.restart.visible = true;
                console.log(scene.steps);
            }
        }

        this.initDeck = (playerDeck) => {
            scene.playerDeck = playerDeck;
            console.log(scene.playerDeck);
         }

        this.joinZone = (users) => {
            for (let i=0; i<users.length; i++) {
                if (users[i].name === scene.you) {
                    scene.text[`field${i+1}Text`].setText(`${scene.zoneData['field'+(i+1)].name}[${users[i].name}]-你`) 
                }
                else {
                    scene.text[`field${i+1}Text`].setText(`${scene.zoneData['field'+(i+1)].name}(${users[i].name})`);
                }

                scene.zoneData['field'+(i+1)].user = users[i];
            }
            scene.text.start.setText(users.length + '/6');
            if (users.length >= 6) {
                scene.text.start.setText('開始');
                scene.text.start.setInteractive();
            }
        }

        this.getUserZone = (userName) => {
            for (let key in scene.zoneData) {
                if (scene.zoneData[key].user.name == userName) {
                    return key;
                }
            }
            return false;
        }

        this.createCard = (field, cardData) => {
            let image = 'cyanCardBack';

            if (scene.zoneData[field].user.name == scene.you) {
                image = cardData.image;
            }
            pCard = playerCard.render(scene.zone[field].x, scene.zone[field].y, image).setInteractive();
            pCard.cardData = cardData;
            scene.zoneData[field].cards.push(pCard);
        }

        this.destroyCard = (fromField, cardData) => {
            for (let i=0; i<scene.zoneData[fromField].cards.length; i++) {
                if (scene.zoneData[fromField].cards[i].cardData.name == cardData.name) {
                    scene.zoneData[fromField].cards[i].destroy();
                    scene.zoneData[fromField].cards.splice(i, 1);
                }
            }
        }

        this.revealCard = () => {
            for (let f in scene.zoneData) {
                console.log(scene.zoneData[f]);
                if (f != 'guess') {
                    try {
                        scene.zoneData[f].cards[0].setTexture(scene.zoneData[f].cards[0].cardData.image).setDisplaySize(150, 210);
                    }
                    catch (e) {
                        scene.zoneData[f].cards[1].setTexture(scene.zoneData[f].cards[1].cardData.image).setDisplaySize(150, 210);
                    }
                }
            }
        }

        this.kill = (killList) => {
            console.log(killList);
            killList.forEach(element => {
                scene.tweens.add({
                    targets: scene.zoneData[element].cards[0],
                    x: scene.zone['guess'].x,
                    y: scene.zone['guess'].y,
                    duration: 1000,
                    onComplete: function () { console.log('onComplete'); console.log(arguments); },
                });
            });
            for (let i=0;i<killList.length;i++) {
                scene.tweens.add({
                    targets: scene.zoneData[killList[i]].cards[0],
                    x: scene.zone['guess'].x - (-i*60),
                    y: scene.zone['guess'].y,
                    duration: 1000,
                    onComplete: function () { console.log('onComplete'); console.log(arguments); },
                });
            }
        }
    }
}