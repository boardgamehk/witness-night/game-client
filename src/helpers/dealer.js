import Card from './card'

export default class Dealer {
    constructor(scene) {
        let playerCard = new Card(scene);
        let pCard;
        
        this.drawCard = (user, num) => { 
            let selfZone = scene.zone.getUserZone(user);
            let selfZoneName = selfZone.data.name;
            let image = 'cyanCardBack';
            for (let i=0;i<num;i++) {
                if (user == scene.you) {
                    image = scene.playerDeck[0].image;
                }
                pCard = playerCard.render(selfZone.x, selfZone.y, image).setInteractive();
                pCard.cardData = scene.playerDeck[0];
                scene.zone[selfZoneName].data.values.cards.push(pCard);
                scene.playerDeck.shift();
            }
            scene.zone[selfZoneName].data.values.cards[0].x = scene.zone[selfZoneName].data.values.cards[0].x - 80;
            scene.zone[selfZoneName].data.values.cards[1].x = scene.zone[selfZoneName].data.values.cards[0].x + 160;
        }
        this.destroyCard = (zoneName, prevUser, gameObject) => { 
 
            let prevUserZoneName = scene.zone.getUserZone(prevUser).data.name;
            for (let i=0; i<scene.zone[prevUserZoneName].data.values.cards.length; i++) {
                if (scene.zone[prevUserZoneName].data.values.cards[i].cardData.name == gameObject.name) {
                    scene.zone[prevUserZoneName].data.values.cards[i].destroy();
                    scene.zone[prevUserZoneName].data.values.cards.splice(i, 1);
                }
            }
            console.log(this.isEnd());
        }
        this.isEnd = () => {
            let end = true;
            scene.fields.forEach(field => {
                if (scene.zone[field].data.values.cards.length == 0) {
                    end = false;
                }
            });
            return end;
        }
    }
}