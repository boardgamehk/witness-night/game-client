export default class Zone {
    constructor(scene) {

        scene.add.image(960, 540, 'bg.jpg').setDisplaySize(1920, 1080);

        this.renderImageZone = (x, y, width, height, image) => {
            let dropZone = scene.add.image(x, y, image).setDisplaySize(width, height).setInteractive();
            dropZone.input.dropZone = true;
            dropZone.setData({cards: []});
            dropZone.setData({cardNum: 0});
            dropZone.setData({vote: 0});
            return dropZone;
        };
        
        this.renderZone = (x, y, width, height) => {
            let dropZone = scene.add.zone(x, y, width, height).setRectangleDropZone(width, height);
            dropZone.setData({cards: []});
            dropZone.setData({cardNum: 0});
            dropZone.setData({vote: 0});
            return dropZone;
        };
        this.renderOutline = (dropZone) => {
            let dropZoneOutline = scene.add.graphics();
            dropZoneOutline.lineStyle(4, 0xff69b4);
            dropZoneOutline.strokeRect(dropZone.x - dropZone.input.hitArea.width / 2, dropZone.y - dropZone.input.hitArea.height / 2, dropZone.input.hitArea.width, dropZone.input.hitArea.height)
        }

        this.joinZone = (users) => {
            for (let i=0; i<users.length; i++) {
                users[i].name === scene.you ? scene.text[`${scene.fields[i]}Text`].setText(`${scene.rooms[i]}(${users[i].name})-You`) : scene.text[`${scene.fields[i]}Text`].setText(`${scene.rooms[i]}(${users[i].name})`)
                scene.zone[scene.fields[i]].data.values.user = users[i];
            }
        }
        this.getUserZone = (user) => {
            let selfZone;
            for (let i=0;i<scene.fields.length;i++) {
                if (scene.zone[scene.fields[i]].data.values.user.name == user) {
                    selfZone = scene.zone[scene.fields[i]];
                    break;
                }
            }
            return selfZone;
        }

        // this.field1 = this.renderZone(scene.startX, scene.startY, scene.cardW, scene.cardH);
        // this.field1Outline = this.renderOutline(this.field1);
        this.field1 = this.renderImageZone(scene.startX, scene.startY, scene.cardW, scene.cardH, 'field1.png');
        this.field1.data.name = "field1";
        // scene.field1.disableInteractive();

        // this.field2 = this.renderZone(scene.startX + (scene.gap + scene.cardW), scene.startY, scene.cardW, scene.cardH);
        // this.field2Outline = this.renderOutline(this.field2);
        this.field2 = this.renderImageZone(scene.startX + (scene.gap + scene.cardW), scene.startY, scene.cardW, scene.cardH, 'field2.png');
        this.field2.data.name = "field2";
        // scene.field2.disableInteractive();

        // this.field3 = this.renderZone(scene.startX + 2*(scene.gap + scene.cardW), scene.startY, scene.cardW, scene.cardH);
        // this.field3Outline = this.renderOutline(this.field3);
        this.field3 = this.renderImageZone(scene.startX + 2*(scene.gap + scene.cardW), scene.startY, scene.cardW, scene.cardH, 'field3.png');
        this.field3.data.name = "field3";
        // scene.field3.disableInteractive();

        // this.field4 = this.renderZone(scene.startX, scene.startY + 2*(scene.gap + scene.cardH), scene.cardW, scene.cardH);
        // this.field4Outline = this.renderOutline(this.field4);
        this.field4 = this.renderImageZone(scene.startX, scene.startY + 2*(scene.gap + scene.cardH), scene.cardW, scene.cardH, 'field4.png');
        this.field4.data.name = "field4";
        // scene.field4.disableInteractive();

        // this.field5 = this.renderZone(scene.startX + (scene.gap + scene.cardW), scene.startY + 2*(scene.gap + scene.cardH), scene.cardW, scene.cardH);
        // this.field5Outline = this.renderOutline(this.field5);
        this.field5 = this.renderImageZone(scene.startX + (scene.gap + scene.cardW), scene.startY + 2*(scene.gap + scene.cardH), scene.cardW, scene.cardH, 'field5.png');
        this.field5.data.name = "field5";
        // scene.field5.disableInteractive();

        // this.field6 = this.renderZone(scene.startX + 2*(scene.gap + scene.cardW), scene.startY + 2*(scene.gap + scene.cardH), scene.cardW, scene.cardH);
        // this.field6Outline = this.renderOutline(this.field6);
        this.field6 = this.renderImageZone(scene.startX + 2*(scene.gap + scene.cardW), scene.startY + 2*(scene.gap + scene.cardH), scene.cardW, scene.cardH, 'field6.png');
        this.field6.data.name = "field6";
        // scene.field6.disableInteractive();

        // this.field7 = this.renderZone(scene.startX - scene.cardW + scene.gap, scene.startY + 1*(scene.gap + scene.cardH), scene.cardW, scene.cardH);
        // this.field7Outline = this.renderOutline(this.field7);
        this.field7 = this.renderImageZone(scene.startX - scene.cardW + scene.gap, scene.startY + 1*(scene.gap + scene.cardH), scene.cardW, scene.cardH, 'field7.png');
        this.field7.data.name = "field7";
        // scene.field7.disableInteractive();

        // this.guess = this.renderZone(scene.startX + 3*(scene.gap + scene.cardW) - scene.gap, scene.startY + 1*(scene.gap + scene.cardH), scene.cardW, scene.cardH);
        // this.guessOutline = this.renderOutline(this.guess);
        this.guess = this.renderImageZone(scene.startX + 3*(scene.gap + scene.cardW) - scene.gap, scene.startY + 1*(scene.gap + scene.cardH), scene.cardW, scene.cardH, 'guess.png');
        this.guess.data.name = "guess";
        this.guess.disableInteractive();

    }
}