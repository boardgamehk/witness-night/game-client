export default class Text {
    constructor(scene) {


        this.renderText = (text, x, y) => {
            let addedText = scene.add.text(x, y, [text]).setFontSize(48).setFontFamily('Trebuchet MS').setColor('#ffffff').setInteractive();
            addedText.on('pointerover', function () {
                addedText.setColor('#00ffff');
            })
            
            addedText.on('pointerout', function () {
                addedText.setColor('#ffffff');
            })
            
            return addedText;
        };

        this.allTextInteractive = (on) => {
            if (on) {
                for (let i=1;i<=7;i++) {
                    scene.text[`field${i}Text`].setInteractive();
                }
            }
            else {
                for (let i=1;i<=7;i++) {
                    scene.text[`field${i}Text`].disableInteractive();
                }
            }
        }

        this.field1Text = this.renderText('畫廊', scene.startX - 175, scene.startY - 180);
        this.field1Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field1';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field1Text.disableInteractive();
        
        this.field2Text = this.renderText('交誼廳', scene.startX + (scene.gap + scene.cardW) - 175, scene.startY - 180);
        this.field2Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field2';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field2Text.disableInteractive();

        this.field3Text = this.renderText('玄關', scene.startX + 2*(scene.gap + scene.cardW) - 175, scene.startY - 180);
        this.field3Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field3';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field3Text.disableInteractive();

        this.field4Text = this.renderText('撞球室', scene.startX - 175, scene.startY + 2*(scene.gap + scene.cardH) - 180);
        this.field4Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field4';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field4Text.disableInteractive();

        this.field5Text = this.renderText('餐廳', scene.startX + (scene.gap + scene.cardW) - 175, scene.startY + 2*(scene.gap + scene.cardH) - 180);
        this.field5Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field5';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field5Text.disableInteractive();

        this.field6Text = this.renderText('書房', scene.startX + 2*(scene.gap + scene.cardW) - 175, scene.startY + 2*(scene.gap + scene.cardH)- 180);
        this.field6Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field6';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field6Text.disableInteractive();

        this.field7Text = this.renderText('客房', scene.startX - scene.cardW + scene.gap - 175, scene.startY + 1*(scene.gap + scene.cardH) - 180);
        this.field7Text.on('pointerdown', function () {
            let obj = {};
            obj.user = scene.you;
            obj.field = 'field7';
            scene.socket.emit('action', 'vote', obj);
        });
        this.field7Text.disableInteractive();

        this.guess = this.renderText('鍋爐室', scene.startX + 3*(scene.gap + scene.cardW) - scene.gap - 175, scene.startY + 1*(scene.gap + scene.cardH)- 180);
        this.guess.disableInteractive();

        this.start = this.renderText('0/6', scene.startX + scene.cardW, scene.startY + scene.startY + 1*(scene.gap + scene.cardH) - 210);
        this.start.on('pointerdown', function () {
            scene.deck.initDeck();
            console.log(scene.playerDeck);
            scene.socket.emit('startGame', scene.playerDeck, scene.you);
            scene.text.start.visible = false;
        })
        this.start.disableInteractive();

        this.restart = this.renderText('重新開始', scene.startX + scene.cardW, scene.startY + scene.startY + 1*(scene.gap + scene.cardH) - 210);
        this.restart.on('pointerdown', function () {
            // scene.deck.initDeck();
            // console.log(scene.playerDeck);
            scene.socket.emit('action', 'restartGame');
            console.log(scene.steps);
            // scene.text.start.visible = false;
        });
        this.restart.visible = false;

        this.vote = this.renderText('投票', scene.startX + scene.cardW, scene.startY + scene.startY + 1*(scene.gap + scene.cardH) - 210);
        this.vote.disableInteractive();
        this.vote.visible = false;

        this.voteNum = this.renderText('0/6', scene.startX + scene.cardW + 100, scene.startY + scene.startY + 1*(scene.gap + scene.cardH) - 210);
        this.voteNum.disableInteractive();
        this.voteNum.visible = false;

        this.gameDesc = this.renderText('', scene.startX + scene.cardW - 300, scene.startY + scene.startY + 1*(scene.gap + scene.cardH) - 210);
        this.gameDesc.disableInteractive();
    }
 
}
