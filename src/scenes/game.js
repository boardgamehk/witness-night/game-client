import io from 'socket.io-client';
import Card from '../helpers/card';
import Dealer from "../helpers/dealer";
import Zone from '../helpers/zone';
import Text from '../helpers/text';
import Deck from '../helpers/deck';
import Action from '../helpers/action';

export default class Game extends Phaser.Scene {
    constructor() {
        super({
            key: 'Game'
        });
    }
    

    preload() {
        this.load.image('cyanCardBack', 'https://hkbgstor.blob.core.windows.net/witness-night/card-back.PNG');
        this.load.image('bg.jpg', 'https://hkbgstor.blob.core.windows.net/witness-night/bg.jpg');

        this.load.image('guest.png', 'https://hkbgstor.blob.core.windows.net/witness-night/guest.PNG');
        this.load.image('guestr.png', 'https://hkbgstor.blob.core.windows.net/witness-night/guestr.PNG');
        this.load.image('guestb.png', 'https://hkbgstor.blob.core.windows.net/witness-night/guestb.PNG');
        this.load.image('killer.png', 'https://hkbgstor.blob.core.windows.net/witness-night/killer.PNG');
        this.load.image('accomplice.png', 'https://hkbgstor.blob.core.windows.net/witness-night/accomplice.PNG');
        this.load.image('bomber.png', 'https://hkbgstor.blob.core.windows.net/witness-night/bomber.PNG');

        this.load.image('field1.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field1.PNG');
        this.load.image('field2.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field2.PNG');
        this.load.image('field3.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field3.PNG');
        this.load.image('field4.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field4.PNG');
        this.load.image('field5.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field5.PNG');
        this.load.image('field6.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field6.PNG');
        this.load.image('field7.png', 'https://hkbgstor.blob.core.windows.net/witness-night/field7.PNG');
        this.load.image('guess.png', 'https://hkbgstor.blob.core.windows.net/witness-night/guess.PNG');

        const deck = require('../assets/ExampleDeck.json');
        deck.forEach(element => {
            this.load.image(element.image, element.image);
        });
    }

    create() {
        this.serverUrl = process.env.SERVER_URL || 'http://localhost:3000';
        console.log(this.serverUrl)

        this.startX = 490;
        this.startY = 200;
        this.cardH = 250;
        this.cardW = 350;
        this.gap = 80;

        this.actions = {};
        this.zoneData = {
            'field1': {"user":"", "cards":[], "name":"畫廊", "vote":0},
            'field2': {"user":"", "cards":[], "name":"交誼廳", "vote":0},
            'field3': {"user":"", "cards":[], "name":"玄關", "vote":0},
            'field4': {"user":"", "cards":[], "name":"撞球室", "vote":0},
            'field5': {"user":"", "cards":[], "name":"餐廳", "vote":0},
            'field6': {"user":"", "cards":[], "name":"書房", "vote":0},
            'field7': {"user":"", "cards":[], "name":"客房", "vote":0},
            'guess': {"user":"", "cards":[], "name":"鍋爐室", "vote":0}
        };
        this.end = false;
        this.you;
        this.playerDeck = [];
        this.steps = [];

        this.zone = new Zone(this);
        this.text = new Text(this);
        this.deck = new Deck(this);
        this.dealer = new Dealer(this);
        this.action = new Action(this);
        var self = this;
        console.log(self);

        this.socket = io(this.serverUrl);

        this.socket.on('connect', function () {
            console.log('Connected!');
            self.socket.on('request register', function (data) {
                var urlParams = new URLSearchParams(window.location.search);
                var token = urlParams.get('token')
                self.socket.emit('register', { 'token': token });

                let _self = self;
                self.socket.once('register result', function (data) {
                    console.log('Register ' + data.result + ': ' + data.reason);
                    if (data.result != 'success') {
                        // Register failed
                        _self.socket.disconnect();
                        _self.socket = null;

                        // Pop error message: data.reason
                        var name = urlParams.get('name');
                        var queryStr = '';
                        if (name && name.trim().length > 0) {
                            queryStr = '?name=' + name;
                        }
                        // window.alert(data.reason);
                        // window.location.replace(_self.serverUrl + '/RoomList' + queryStr);
                        // let tempURL = 'http://localhost:8080';
                        let tempURL = process.env.ROOM_URL || 'http://localhost:8080';
                        window.location.replace(`${tempURL}/#/witness-night/${queryStr}`);
                        return;
                    }

                    //  Register success
                    _self.name = data.name;
                    _self.roomName = data.roomName;
                    _self.isPlayerA = data.isPlayerA;
                    _self.users = data.users;
                    _self.you = data.name;
                    self.socket.emit('action', 'joinZone', data.users);
                    if (data.users.length == 6) {
                        console.log('full');
                    }
                });
            });
        });

        this.socket.on('startGame', function (deck, user) {
            self.playerDeck = deck;
            self.text.start.visible = false; 
            if (user === self.you) {
                let obj = {};
                obj.user = user;
                obj.num = 2;
                self.playerDeck = self.playerDeck.sort(() => Math.random() - 0.5);
                console.log(self.playerDeck);
                self.socket.emit('action', 'initDeck', self.playerDeck);
                self.socket.emit('action', 'drawCard', obj);
            }
        })

        this.socket.on('action', function (actionName, obj) {
            console.log(actionName);
            console.log(obj);
            self.action[actionName](obj);
            self.steps.push([actionName, obj]);
        })

        this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
            gameObject.x = dragX;
            gameObject.y = dragY;
        })

        this.input.on('dragstart', function (pointer, gameObject) {
            // gameObject.setTint(0xff69b4);
            self.children.bringToTop(gameObject);
        })

        this.input.on('dragend', function (pointer, gameObject, dropped) {
            if (!dropped) {
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;
            }
        })

        this.input.on('drop', function (pointer, gameObject, dropZone) {
            gameObject.x = dropZone.x;
            gameObject.y = dropZone.y;

            let obj = {};
            obj.fromField = self.action.getUserZone(self.you);
            obj.toField = dropZone.data.name;
            obj.cardData = gameObject.cardData;
            if (dropZone.data.name != self.action.getUserZone(self.you)) {
                self.socket.emit('action', 'moveCard', obj);
            }
            else {
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;
            }
        })
    }

    update() {

    }

}