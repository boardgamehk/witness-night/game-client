# Game Client

## Build options:

1. Local

```
npm install
npm start
docker-compose up
```

## Build Docker image

Uncomment wn-game in docker-compose.yml
```
docker build -t wn-game-client .
docker-compose up
```

## Test locally
1. Open http://localhost:8080/#/witness-night
2. Create a room
3. Open another 5 tabs and join the room
4. Start game

## Docker GitLab runner config
```
nano ~/.gitlab-runner/config.toml
```

add
```
privileged = true
volumes = ["/cache","/var/run/docker.sock:/var/run/docker.sock"]
```